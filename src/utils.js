import $ from 'jquery';

export function populateTable(tableContainer, data) {
    data.forEach(item => {
        const row = $('<tr />');
        row.data('rowData', item);
        row.html(`
            <td>${item.name} <button class="show-more btn btn-outline-info btn-sm">Visualize</button></td>
            <td>${item.protein}</td>
            <td>${item.fat}</td>
            <td>${item.carbohydrate}</td>
            <td>${item.kcal}</td>
        `);
        tableContainer.append(row);
    });
}