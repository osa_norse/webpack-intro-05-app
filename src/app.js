import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.css';
import './app.css';

import { populateTable } from './utils';
import { renderGraph } from './graph-lib';
import Data from './data';

const tableBody = $('#table-body');
const graphContainer = $('#graph');

populateTable(tableBody, Data);

tableBody.on('click', '.show-more', (e) => {
    const row = $(e.target).closest('tr');
    renderGraph(graphContainer.get(0), row.data('rowData'));
});
