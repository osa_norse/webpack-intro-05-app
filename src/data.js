export default [
    {
        name: 'Торт Вивальди',
        protein: 4.3,
        fat: 12.8,
        carbohydrate: 52.6,
        kcal: 340
    },
    {
        name: 'Торт Ёжик',
        protein: 4.1,
        fat: 25,
        carbohydrate: 50.9,
        kcal: 445
    },
    {
        name: 'Торт Крепвиль Ле Нуар',
        protein: 3.4,
        fat: 17.9,
        carbohydrate: 36.1,
        kcal: 322
    }
];
