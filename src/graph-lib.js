import Highcharts from 'highcharts';

export function renderGraph(container, data) {
    Highcharts.chart(container, {
        chart: {
            type: 'pie'
        },
        title: {
            text: data.name
        },
        subtitle: {
            text: `${data.kcal} ккал`
        },
        series: [{
            name: data.name,
            data: [{
                name: 'Белки, г',
                y: data.protein
            }, {
                name: 'Жиры, г',
                y: data.fat
            }, {
                name: 'Углеводы, г',
                y: data.carbohydrate
            }]
        }]
    });
}